# persisting-time

### Or what do you do when you have a problem in production, no logs and the data has changed?


I'm a painter and last week I launched a new offer for 20% off, that includes two of my paintings. 
I work with a gallery in France and one morning I receive a few messages:

 ![alt text](conversation.jpg)
 
The owner is very upset. So let's start our detective work:

First, let's see the offer now.


```
curl -X GET --header 'Accept: application/json' 'http://localhost:3000/api/offer/offer%7Ctwenty'

{
  "offer|twenty": {
    "id": "offer|twenty",
    "name": "20% off",
    "status": false,
    "paintings": {
      "painting|imagine": {
        "id": "painting|imagine",
        "name": "Imagine",
        "status": "sold"
      },
      "painting|flipando": {
        "id": "painting|flipando",
        "name": "Flipando",
        "status": "sold"
      }
    }
  }
}
```

Hmm, sold with two paintings...
I launched it last tuesday, which was the 1st of Oct, I think it had two paintings...


```
curl -X GET --header 'Accept: application/json' 'http://localhost:3000/api/offer/offer%7Ctwenty/as-of/2019-10-01'

{
  "offer|twenty": {
    "id": "offer|twenty",
    "name": "20% off",
    "status": true,
    "paintings": {
      "painting|imagine": {
        "id": "painting|imagine",
        "name": "Imagine",
        "status": "available"
      },
      "painting|flipando": {
        "id": "painting|flipando",
        "name": "Flipando",
        "status": "available"
      }
    }
  }
}
```

So Friday was The 4th

```
curl -X GET --header 'Accept: application/json' 'http://localhost:3000/api/offer/offer%7Ctwenty/as-of/2019-10-04'

{
  "offer|twenty": {
    "id": "offer|twenty",
    "name": "20% off",
    "status": true,
    "paintings": {
      "painting|flipando": {
        "id": "painting|flipando",
        "name": "Flipando",
        "status": "available"
      }
    }
  }
}
```

Hmmm, she's right, Friday the system had a single painting...
Maybe we should check the history:

```
curl -X GET --header 'Accept: application/json' 'http://localhost:3000/api/offer-history/offer%7Ctwenty/as-of/2019-10-01'

{
  "offer-id": "offer|twenty",
  "start-time": "2019-10-01T00:00:00Z",
  "database-results": {
    "2019-10-01T00:00:00.000+0000": {
      "crux.db/id": "offer|twenty",
      "offer/id": "offer|twenty",
      "offer/name": "20% off",
      "offer/active": true,
      "offer/percentage": 20,
      "offer/paintings": [
        "painting|flipando",
        "painting|imagine"
      ],
      "offer/user": "user/dan"
    },
    "2019-10-03T00:00:00.000+0000": {
      "crux.db/id": "offer|twenty",
      "offer/id": "offer|twenty",
      "offer/name": "20% off",
      "offer/active": true,
      "offer/percentage": 20,
      "offer/paintings": [
        "painting|flipando"
      ],
      "offer/user": "user|anca"
    },
    "2019-10-06T00:00:00.000+0000": {
      "crux.db/id": "offer|twenty",
      "offer/id": "offer|twenty",
      "offer/name": "20% off",
      "offer/active": true,
      "offer/percentage": 20,
      "offer/paintings": [
        "painting|flipando",
        "painting|imagine"
      ],
      "offer/user": "user|anca"
    },
    "2019-10-07T00:00:00.000+0000": {
      "offer/user": "user|alex",
      "offer/client-id": "client|johndoe",
      "offer/percentage": 20,
      "offer/paintings": [
        "painting|flipando",
        "painting|imagine"
      ],
      "offer/reason": "sold",
      "offer/name": "20% off",
      "offer/active": false,
      "offer/id": "offer|twenty",
      "crux.db/id": "offer|twenty"
    }
  }
}
```

Oooh, it seem Anca (user/anca) changed the offer on the 3rd (Thursday), then reverted it on the 6 (Monday). Then it was sold on the 7th. 

## Now why was this all possible?

- making queries back in time
- viewing the entire history of something?

 

## Usage

### Run the application locally

`lein ring server`

### Run the tests

`lein test`

### Packaging and running as standalone jar

```
lein do clean, ring uberjar
java -jar target/server.jar
```

### Packaging as war

`lein ring uberwar`

## License

Copyright ©  FIXME
