(ns persisting-time.handler
  (:require [compojure.api.sweet :refer :all]
            [ring.util.http-response :refer :all]
            [schema.core :as s]
            [persisting-time.use-cases.offers :as offers-use-case]
            [clojure.instant :refer [read-instant-date]]
            [persisting-time.events :as events]))



(def app
  (api
    {:swagger
     {:ui   "/"
      :spec "/swagger.json"
      :data {:info {:title       "Persisting-time"
                    :description "Compojure Api example"}
             :tags [{:name "api", :description "some apis"}]}}}

    (context "/api" []
      :tags ["api"]

      (GET "/offer/:offer-id" []
        :path-params [offer-id :- s/Str]
        :summary "returns an offer by id"
        (ok (events/do! :offer/execute {:offer-id (keyword offer-id) :time nil})))

      (GET "/offer/:offer-id/as-of/:time" []
        :path-params [offer-id :- s/Str
                      time :- s/Str]
        :summary "returns an offer by id. And by TIME"
        (ok (events/do! :offer/execute {:offer-id (keyword offer-id) :time (when time (read-instant-date time))})))

      (GET "/offer-history/:offer-id/as-of/:start-time" []
        :path-params [offer-id :- s/Str
                      start-time :- s/Str]
        :summary "returns an offer history by id starting at"
        (ok (events/do! :offer/history {:offer-id (keyword offer-id) :start-time (read-instant-date start-time)})))
      )))
