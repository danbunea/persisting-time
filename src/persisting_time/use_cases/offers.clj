(ns persisting-time.use-cases.offers
  (:require
    [persisting-time.dao.database-setup :refer [create-history]]
    [persisting-time.dao.offers-dao :refer [offer-by-id offer-history]]
    [persisting-time.events :as events]
    [clojure.instant :refer [read-instant-date]]
    [clojure.spec.alpha :as s]
    ))



(defn collect-row [state row]
  (-> state
      (assoc-in [(get row 0) :id] (get row 0))
      (assoc-in [(get row 0) :name] (get row 1))
      (assoc-in [(get row 0) :status] (get row 2))
      (assoc-in [(get row 0) :paintings (get row 3) :id] (get row 3))
      (assoc-in [(get row 0) :paintings (get row 3) :name] (get row 4))
      (assoc-in [(get row 0) :paintings (get row 3) :status] (get row 5))

      ))
(defn collect-rows [state]
  (->> (:database-results state)
       (reduce
         collect-row
         {})))

(defn query-db [state]
  (let [{offer-id :offer-id time :time} state]
    (assoc state :database-results (offer-by-id offer-id time))))

(defn query-db-history [state]
  (let [{:keys [offer-id start-time]} state]
    (assoc state :database-results (offer-history offer-id start-time))))


(events/register-events! [{:id       :offer/execute
                           :params   {:offer-id keyword?
                                      :time     #(or (inst? %) (nil? %))
                                      }
                           :pipeline [
                                      query-db
                                      collect-rows
                                      ]}
                          {:id       :offer/history
                           :params   {:offer-id   keyword?
                                      :start-time inst?
                                      }
                           :pipeline [
                                      query-db-history
                                      ]}])



(comment

  (events/do! :offer/execute {:offer-id :offer|twenty
                               :time     #inst "2019-10-03T00:00:00.000-00:00"})

  (events/do! :offer/execute {:offer-id :offer|twenty
                               :time     nil})

  (events/do! :offer/history {:offer-id :offer|twenty
                               :start-time     #inst "2019-10-01T00:00:00.000-00:00"})

  )