(ns persisting-time.dao.database
  (:require [crux.api :as crux])
  (:import [crux.api ICruxAPI]))




(def ^crux.api.ICruxAPI node
  (crux/start-node {:crux.node/topology :crux.standalone/topology
                    :crux.node/kv-store "crux.kv.memdb/kv"
                    :crux.kv/db-dir "data/db-dir-1"
                    :crux.standalone/event-log-dir "data/eventlog-1"
                    :crux.standalone/event-log-kv-store "crux.kv.memdb/kv"}))

;(def crux
;  (crux/start-standalone-node
;  {:kv-backend    "avisi.crux.kv.xodus.XodusKv"
;   :db-dir        "data/db-dir-1"
;   :event-log-dir "data/eventlog-1"}))

;(def ^crux.api.ICruxAPI node
;  (crux/start-standalone-node {:kv-backend "crux.kv.rocksdb.RocksKv"
;                               :db-dir "data/db-dir-1"
;                               :event-log-dir "data/eventlog-1"}))

;(def ^crux.api.ICruxAPI crux
;  (crux/start-jdbc-node {:dbtype "mysql"
;                         :dbname "cruxdb"
;                         :host "127.0.0.1"
;                         :port 3307
;                         :user "root"
;                         :password "qwerasdf"}))

