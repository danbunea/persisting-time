(ns persisting-time.dao.offers-dao
  (:require [persisting-time.dao.database :refer [node]]
            [crux.api :as crux]))


(defn offer-by-id
  ([offer-id] (offer-by-id offer-id nil))
  ([offer-id time]
   (crux/q (crux/db node time)
           {
            :find  '[offer-id offer-name offer-active
                     painting-id painting-name painting-status]
            :where '[[offer :offer/id offer-id]
                     [painting :painting/id painting-id]
                     [offer :offer/paintings painting-id]
                     [offer :offer/name offer-name]
                     [offer :offer/active offer-active]
                     [painting :painting/name painting-name]
                     [painting :painting/price painting-price]
                     [painting :painting/status painting-status]
                     ]
            :args  [{'offer-id offer-id}]})
    ))

(defn offer-history [offer-id start-time]
  (->> (crux/history-ascending
         (crux/db node start-time)
         (crux/new-snapshot (crux/db node start-time))
         offer-id)
       (reduce
         #(assoc %1 (:crux.db/valid-time %2) (:crux.db/doc %2))
         {})
       ))


(comment
  (offer-by-id :offer|twenty)
  (offer-by-id :offer|twenty #inst "2019-10-03T00:00:00.000-00:00")
  (offer-history :offer|twenty #inst "2019-10-01T00:00:00.000-00:00")
  )