(ns persisting-time.dao.database-setup
  (:require [persisting-time.dao.database :refer [node]]
            [crux.api :as crux]))




(defn easy-ingest
  "Uses Crux put transaction to add a vector of documents to a specified
  node"
  [node docs]
  (crux/submit-tx node
                  (vec (for [doc docs]
                         [:crux.tx/put doc]))))


(def date-added #inst "2019-10-01T00:00:00.000-00:00")
(def date-last-thu #inst "2019-10-03T00:00:00.000-00:00")
(def date-last-friday #inst "2019-10-04T00:00:00.000-00:00")
(def date-monday #inst "2019-10-06T00:00:00.000-00:00")
(def date-tuesday #inst "2019-10-07T00:00:00.000-00:00")



(def painting-flipando {:crux.db/id      :painting|flipando
                        :painting/id     :painting|flipando
                        :painting/name   "Flipando"
                        :painting/status "available"
                        :painting/price  1000
                        :painting/user   :user/dan
                        })

(def painting-imagine {:crux.db/id      :painting|imagine
                       :painting/id     :painting|imagine
                       :painting/name   "Imagine"
                       :painting/status "available"
                       :painting/price  800
                       :painting/user   :user/dan
                       })


(def offer-20 {:crux.db/id       :offer|twenty
               :offer/id         :offer|twenty
               :offer/name       "20% off"
               :offer/active     true
               :offer/percentage 20
               :offer/paintings  #{:painting|flipando :painting|imagine}
               :offer/user       :user/dan
               })



(def offer-mistakinly-changed (-> offer-20
                                 (assoc :offer/paintings #{:painting|flipando})
                                 (assoc :offer/user :user|anca)
                                 ))
(def offer-restored (-> offer-mistakinly-changed
                        (assoc :offer/active true)
                        (assoc :offer/paintings #{:painting|flipando :painting|imagine})
                        (assoc :offer/user :user|anca)
                        ))
(def offer-sold (-> offer-restored
                    (assoc :offer/active false)
                    (assoc :offer/reason "sold")
                    (assoc :offer/user :user|alex)
                    (assoc :offer/client-id :client|johndoe)
                    ))


(defonce create-history
         (do
           (crux/submit-tx node [[:crux.tx/put painting-flipando date-added]])
           (crux/submit-tx node [[:crux.tx/put painting-imagine date-added]])

           (crux/submit-tx node [[:crux.tx/put offer-20 date-added]])
           (crux/submit-tx node [[:crux.tx/put offer-mistakinly-changed date-last-thu]])
           (crux/submit-tx node [[:crux.tx/put offer-restored date-monday]])
           (crux/submit-tx node [[:crux.tx/put offer-sold date-tuesday]])

           (crux/submit-tx node [[:crux.tx/put (assoc painting-flipando :painting/status "sold") date-tuesday]])
           (crux/submit-tx node [[:crux.tx/put (assoc painting-imagine :painting/status "sold") date-tuesday]])
           )
         )

(comment
  (crux/entity (crux/db node) :painting|flipando)
  (crux/entity (crux/db node) :offer|twenty)
  )
