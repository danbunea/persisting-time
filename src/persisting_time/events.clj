(ns persisting-time.events
  (:require [spec-tools.data-spec :as ds]
            [clojure.spec.alpha :as s]
            [clojure.core.match :as match]
            [clojure.string :as string]))


(defonce events (atom {}))

(s/def :tada/event
  (ds/spec
    {:name :tada/event
     :spec {:params   {keyword? (ds/or {:keyword keyword?
                                        :fn      fn?
                                        :spec    s/spec?})}
            :pipeline [fn?]                                 ;;todo write spec for pipeline functions
            }}))


(s/def :tada/events
  (ds/spec
    {:name :tada/events
     :spec {keyword? :tada/event}}))


(defn- make-event-spec
  [event]
  (ds/spec {:name (keyword "ev-spec" (name (event :id)))
            :spec (event :params)}))

(defn register-events!
  [evs]
  {:pre  [(every? (partial s/valid? :tada/event) evs)]
   :post [(s/valid? :tada/events @events)]}
  (reset!
    events
    (into {}
          (comp
            (map (fn [evt] (assoc evt :params-spec (make-event-spec evt))))
            (map (juxt :id identity)))
          evs)))


(defn pre-cond [condition message current-state function code]
  (when condition
    (throw (ex-info message {:state current-state :function function :code code})))
  )

(defn explain-params-errors [spec value]
  (->> (s/explain-data spec value)
       ::s/problems
       (map (fn [{:keys [path pred val via in]}]
              (match/match [pred]
                           [([fn [_] ([contains? _ missing-key] :seq)] :seq)] {:issue    :missing-key
                                                                               :key-path (conj path missing-key)}
                           [_] {:issue    :incorrect-value
                                :key-path path})))
       (map (fn [{:keys [issue key-path]}]
              (str key-path " " issue)))
       (string/join "\n")))


(defn- validate-input
  [ev state]
  (pre-cond (not (s/valid? (:params-spec ev) state))
            (str "Event params do not meet spec:\n" (explain-params-errors (:params-spec ev) state))
            state
            ::validate-input
            :unsupported)
  (select-keys state (keys (:params ev)))
  )

(defn do! [ev-id params]
  (if-let [ev (ev-id @events)]
    (let [pipeline-fns (:pipeline ev)
          pipeline (apply comp (reverse pipeline-fns))]
      (pipeline (validate-input ev params)))
    (throw (ex-info (str "No event with id " ev-id)
                    {:event-id ev-id
                     :anomaly  :unsupported}))))

(comment
  ;SPEC
  (s/def :bank/currency #{:CAD :USD :EUR})
  (s/def :bank/user
    (ds/spec
      {:name :bank/user
       :spec {:bank.user/id   int?
              :bank.user/name string?}}))

  (s/def :bank/account
    (ds/spec
      {:name :bank/account
       :spec {:bank.account/id       int?
              :bank.account/balance  integer?
              :bank.account/currency :bank/currency
              :bank.account/owners   [:bank.user/id]}}))



  ;PIPELINE
  (defn user-exists? [state]
    (pre-cond (not (:user-id state))
              "User with this id does not exist" state ::user-exists? :forbidden)
    ;...
    state)

  (defn account-exists? [state]
    (pre-cond (not (:account-id state))
              "Account with this id does not exist" state ::account-exists? :not-found)
    ;...
    state)

  (defn user-owns-account? [state]
    (pre-cond (not= (:account-id state) (:user-id state))   ;fake condition
              "User does not own this account" state ::user-owns-account? :forbidden)
    ;...
    state)


  (defn get-account [state]
    (pre-cond (not= :EUR (:currency state))                 ;fake condition
              "Deposit currency must match account" state ::get-account :incorrect)
    (assoc state [:accounts (:account-id state) (:currency state)] 0))


  (defn deposit [state]
    (update-in state [:accounts (:account-id state) (:currency state)] #(+ (or % 0) (:amount state))))

  ;EVENTS
  (reset! events {})
  (register-events! [{:id       :deposit!
                      :params   {:user-id    :bank.user/id
                                 :account-id :bank.account/id
                                 :amount     integer?
                                 :currency   :bank/currency}
                      :pipeline [user-exists? account-exists? user-owns-account? get-account deposit]}])



  (register-events! [{:id       :transfer!
                      :params   {:user-id    :bank.user/id
                                 :account-id :bank.account/id
                                 :amount     integer?
                                 :currency   :bank/currency}
                      :pipeline [user-exists?
                                 account-exists?
                                 user-owns-account?
                                 get-account
                                 check-enough-money?
                                 get-other-account
                                 transfer]}])




  ;CHECK

  (do! :deposit! {})
  (do! :deposit! {:user-id 1})
  (do! :deposit! {:user-id    1
                  :account-id 100})

  (do! :deposit! {:user-id    1
                  :account-id 100
                  :amount     12
                  :currency   :USD
                  })

  (do! :deposit! {:user-id    1
                  :account-id 1
                  :currency   :USD
                  :amount     530})

  (do! :deposit! {:user-id    1
                  :account-id 1
                  :currency   :EUR
                  :amount     500})

  )