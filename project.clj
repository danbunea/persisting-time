 (defproject persisting-time "0.1.0-SNAPSHOT"
   :description "FIXME: write description"
   :dependencies [[org.clojure/clojure "1.10.0"]
                  [metosin/compojure-api "2.0.0-alpha30"]
                  [metosin/spec-tools "0.10.0"]
                  [org.clojure/core.match "0.3.0"]
                  [org.clojure/test.check "0.10.0"]
                  [juxt/crux-core "RELEASE"]
                  ;[juxt/crux-rocksdb "19.09-1.4.0-alpha"]
                  ;[avisi-apps/crux-xodus "0.2.0-SNAPSHOT"]
                  ]
   :ring {:handler persisting-time.handler/app}
   :uberjar-name "server.jar"
   :profiles {:dev {:dependencies [[javax.servlet/javax.servlet-api "3.1.0"]
                                  [ring/ring-mock "0.3.2"]]
                   :plugins [[lein-ring "0.12.5"]]}})
